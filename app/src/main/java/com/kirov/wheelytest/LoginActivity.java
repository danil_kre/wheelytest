package com.kirov.wheelytest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity  implements OnMapReadyCallback {

    private static final String TAG = LoginActivity.class.getName();
    private HashMap<Integer, Marker> markers = new HashMap<Integer, Marker>();

    // UI references.
    //@Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.edittext_username) AutoCompleteTextView mUsernameView;
    @Bind(R.id.password) EditText mPasswordView;
    @Bind(R.id.progress_form) View mProgressView;
    @Bind(R.id.login_form) View mLoginFormView;
    @Bind(R.id.button_connect) Button connectButton;
    @Bind(R.id.button_disconnect) Button disconnectButton;
    @Bind(R.id.map_form) RelativeLayout mapForm;
    @Bind(R.id.toolbar) Toolbar toolBar;
    private GoogleMap mMap;

    private SharedPreferences myPrefs;
    private EnumState currentState = EnumState.STATE_LOGIN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(WSService.INTENT_FILTER));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setSupportActionBar(toolBar);
        setState(EnumState.STATE_LOGIN);
    }

    @Override
    protected void onResume() {
        super.onResume();

        myPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (WSService.isConnected()) {
            setState(EnumState.STATE_MAP);
        }
    }

    @Override
    protected void onPause()
    {
        saveMarkersData();
        super.onPause();
    }

    /**
     * Функция сохранения текущих меток на карте
     */
    private void saveMarkersData()
    {
        SharedPreferences.Editor editor = myPrefs.edit();

        editor.putInt(PreferencesKeys.KEY_MARKERS_COUNT, markers.size());
        int i = 0;
        for (HashMap.Entry<Integer, Marker> entry : markers.entrySet()) {
            editor.putString(PreferencesKeys.KEY_LAT + String.valueOf(i), String.valueOf(entry.getValue().getPosition().latitude));
            editor.putString(PreferencesKeys.KEY_LON + String.valueOf(i), String.valueOf(entry.getValue().getPosition().longitude));
            i++;
        }
        editor.apply();
    }

    /**
     * Функция загрузки текущих
     */
    private void restoreMarkersData()
    {
        int cnt = myPrefs.getInt(PreferencesKeys.KEY_MARKERS_COUNT, 0);
        for (int i=0;i<cnt;i++)
        {
            if (myPrefs.contains(PreferencesKeys.KEY_LAT + String.valueOf(i)) && myPrefs.contains(PreferencesKeys.KEY_LON + String.valueOf(i))) {
                LatLng pos = new LatLng(Double.parseDouble(myPrefs.getString(PreferencesKeys.KEY_LAT + String.valueOf(i), "0")),
                        Double.parseDouble(myPrefs.getString(PreferencesKeys.KEY_LON + String.valueOf(i), "0")));
                Marker marker = mMap.addMarker(new MarkerOptions().position(pos).title(String.valueOf(i)));
                markers.put(i, marker);
            }
        }

    }

    /**
     * Обработчик нажатия на кнопку CONNECT
     */
    @OnClick(R.id.button_connect)
    public void onConnectClick() {
        // Пытаемся соединиться с сервером
        attemptLogin();
        // и запускаем "будильник", чтобы поддерживать связь и слать серверу наши координаты
        startRepeatingAlarm(5); // каждые 5 секунд проверять состояние соединения с сервером
    }

    /**
     * Обработчик нажатия на кнопку DISCONNECT
     */
    @OnClick(R.id.button_disconnect)
    public void onDisconnectClick() {
        cancelRepeatingAlarm(); // отключаем "будильник"
        setState(EnumState.STATE_LOGIN); // UI переводим в состояние ввода логина и пароля
        WSService.disconnect(this); // Просим сервис отцепиться от сервера
    }

    /**
     * Обработчик нажатия на прогрессбар по время подключения к серверу. Нужен, чтобы прекратить попытки подсоединиться к серверу, если он не доступен или не отвечает.
     */
    @OnClick(R.id.progress_form)
    public void onProgressClick() {
        onDisconnectClick();
    }

    /**
     * Смена внешнего состояния активности
     * @param state новое состояние
     */
    private void setState(EnumState state)
    {
        currentState = state;
        switch (state)
        {
            case STATE_LOGIN:
                mProgressView.setVisibility(View.GONE);
                mLoginFormView.setVisibility(View.VISIBLE);
                mapForm.setVisibility(View.GONE);
                setTitle(getString(R.string.title_activity_login));
                break;
            case STATE_MAP:
                mProgressView.setVisibility(View.GONE);
                mLoginFormView.setVisibility(View.GONE);
                mapForm.setVisibility(View.VISIBLE);
                setTitle(getString(R.string.title_activity_map));
                break;
            case STATE_LOAD:
                mProgressView.setVisibility(View.VISIBLE);
                mLoginFormView.setVisibility(View.GONE);
                mapForm.setVisibility(View.GONE);
                setTitle(getString(R.string.title_activity_login));
                break;
        }
    }

    private void attemptLogin() {
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid username.
        if (TextUtils.isEmpty(email)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            WSService.connect(this, email, password);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (WSService.isConnected())
        {
            restoreMarkersData();

            if (!markers.isEmpty()) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (HashMap.Entry<Integer, Marker> entry : markers.entrySet()) {
                    builder.include(entry.getValue().getPosition());
                }
                LatLngBounds bounds = builder.build();
                int padding = 100; // отступ от краёв до наших точек на карте
                try {
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    mMap.moveCamera(cu);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    /**
     *  Приёмник сообщений от сервиса
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String result = intent.getStringExtra(WSService.INTENT_PARAM_RESULT);
            switch (result) {
                case WSService.SERVICE_RESULT_ERROR: // мы получили ошибку
                {
                    if (currentState != EnumState.STATE_LOGIN)
                    {
                        String error = intent.getStringExtra(WSService.INTENT_PARAM_ERROR);
                        if (error.equals(WSService.SERVICE_ERROR_INVALID_LOGIN_PWD)) {
                            mPasswordView.setError(getString(R.string.error_incorrect_password));
                            mPasswordView.requestFocus();

                            setState(EnumState.STATE_LOGIN);
                        } else
                            Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
                    }
                    showProgress(false);
                    cancelRepeatingAlarm();
                }
                break;

                case WSService.SERVICE_RESULT_CONNECT: // произошло соединение с сервером
                {
                    setState(EnumState.STATE_MAP);
                }
                break;

                case WSService.SERVICE_RESULT_DISCONNECT: // произошло отсоединение от сервера
                {
                    setState(EnumState.STATE_LOGIN);
                }
                break;

                case WSService.SERVICE_RESULT_MESSAGE: // от сервера пришло сообщение (с координатами меток)
                {
                    String data = intent.getStringExtra(WSService.INTENT_PARAM_DATA);
                    Log.d(TAG, data);
                    try {
                        markers.clear(); // Чистим существующие метки
                        mMap.clear();

                        // Вытянем из сообщения новые метки
                        JSONArray jsonArray = new JSONArray(data);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject curr = jsonArray.getJSONObject(i);
                            Log.d(TAG, "id=" + curr.getInt(PreferencesKeys.KEY_ID) + " lat=" + curr.getDouble(PreferencesKeys.KEY_LAT));
                            Integer id = curr.getInt(PreferencesKeys.KEY_ID);
                            Double lat = curr.getDouble(PreferencesKeys.KEY_LAT);
                            Double lon = curr.getDouble(PreferencesKeys.KEY_LON);
                            LatLng pos = new LatLng(lat, lon);

                            Marker marker = mMap.addMarker(new MarkerOptions().position(pos).title(String.valueOf(id)));
                            markers.put(id, marker);
                        }

                        // Нарисуем на карте новые метки, если они есть
                        if (!markers.isEmpty()) {
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (HashMap.Entry<Integer, Marker> entry : markers.entrySet()) {
                                builder.include(entry.getValue().getPosition());
                            }
                            LatLngBounds bounds = builder.build();
                            int padding = 100; // отступ от краёв до наших точек на карте
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                            mMap.moveCamera(cu); // двигем камеру так, чтобы было видно все метки
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            }
        }
    };

    /**
     * Функция, которая заводит "будильник" для проверки состояния соединения
     * @param period период проверки в секундах
     */
    public void startRepeatingAlarm(Integer period)
    {
        Log.d(TAG, "startRepeatingAlarm");
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent pi =  PendingIntent.getBroadcast(this, 1, intent, 0);

        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        cancelRepeatingAlarm();

        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, period * 1000, period * 1000, pi);
    }

    /**
     * Функция, отключающая "будильник"
     */
    public void cancelRepeatingAlarm()
    {
        Log.d(TAG, "cancelRepeatingAlarm");
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent pi =  PendingIntent.getBroadcast(this, 1, intent, 0);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        am.cancel(pi);
    }

    /**
     * Приёмник сообщений от "будильника"
     */
    public static class AlarmReceiver extends BroadcastReceiver
    {
        private static final String TAG = AlarmReceiver.class.getName();
        @Override
        public void onReceive(Context context, Intent intent)
        {
            WSService.connect(context);
            Log.d(TAG, "intent = "+intent);
        }
    }
}

