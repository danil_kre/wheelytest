package com.kirov.wheelytest;

/**
 * Created by user on 08.07.16.
 */
public class PreferencesKeys {
    public static final String KEY_STORE_NAME = "WSService";
    public static final String KEY_MARKERS_COUNT = "MARKERS_COUNT";
    public static final String KEY_LAT = "lat";
    public static final String KEY_LON = "lon";
    public static final String KEY_ID = "id";
    public static final String KEY_USERNAME = "USERNAME";
    public static final String KEY_PASSWORD = "PASSWORD";
}
