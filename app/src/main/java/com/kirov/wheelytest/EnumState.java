package com.kirov.wheelytest;

/**
 * Created by user on 08.07.16.
 */
public enum EnumState {
    STATE_LOGIN, // находимся в состоянии ожидания ввода имени пользователя и пароля
    STATE_MAP,   // состояние демонстрации карты
    STATE_LOAD   // состояние соединения с сервером
}
