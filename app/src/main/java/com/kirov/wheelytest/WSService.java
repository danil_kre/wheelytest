package com.kirov.wheelytest;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONObject;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

public class WSService extends IntentService {

    private static final String TAG = WSService.class.getName();
    private static final String ACTION_CONNECT = "com.kirov.wheelytest.action.CONNECT";
    private static final String ACTION_DISCONNECT = "com.kirov.wheelytest.action.DISCONNECT";

    private static final String EXTRA_PARAM_USERNAME = "com.kirov.wheelytest.extra.PARAM_USERNAME";
    private static final String EXTRA_PARAM_PASSWORD = "com.kirov.wheelytest.extra.PARAM_PASSWORD";

    public static final String INTENT_FILTER = "WSSubject";
    public static final String INTENT_PARAM_RESULT = "result";
    public static final String INTENT_PARAM_DATA = "data";
    public static final String INTENT_PARAM_ERROR = "error";

    public static final String SERVICE_RESULT_CONNECT = "CONNECT";
    public static final String SERVICE_RESULT_DISCONNECT = "DISCONNECT";
    public static final String SERVICE_RESULT_MESSAGE = "MESSAGE";
    public static final String SERVICE_RESULT_ERROR = "ERROR";

    public static final String SERVICE_ERROR_INVALID_LOGIN_PWD = "Incorrent login/password";
    public static final String SERVICE_ERROR_SENDING = "Error on sending coordinates";
    public static final String SERVICE_ERROR_HOST_UNRESOLVED = "Host is unresolved";

    private static String saved_username;
    private static String saved_password;

    private static WebSocketConnection mConnection;

    public WSService() {
        super("WSService");
    }

    public static boolean isConnected()
    {
        return mConnection != null && mConnection.isConnected();
    }

    public static void connect(Context context) {
        Intent intent = new Intent(context, WSService.class);
        intent.setAction(ACTION_CONNECT);
        SharedPreferences sp = context.getSharedPreferences(PreferencesKeys.KEY_STORE_NAME, MODE_PRIVATE);
        saved_username = sp.getString(PreferencesKeys.KEY_USERNAME, "");
        saved_password = sp.getString(PreferencesKeys.KEY_PASSWORD, "");
        intent.putExtra(EXTRA_PARAM_USERNAME, saved_username);
        intent.putExtra(EXTRA_PARAM_PASSWORD, saved_password);
        context.startService(intent);
    }

    public static void connect(Context context, String user, String password) {
        SharedPreferences sp = context.getSharedPreferences(PreferencesKeys.KEY_STORE_NAME, MODE_PRIVATE);
        sp.edit().putString(PreferencesKeys.KEY_USERNAME, user).putString(PreferencesKeys.KEY_PASSWORD, password).apply();
        saved_username = user;
        saved_password = password;
        connect(context);
    }

    public static void disconnect(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PreferencesKeys.KEY_STORE_NAME, MODE_PRIVATE);
        sp.edit().remove(PreferencesKeys.KEY_USERNAME).remove(PreferencesKeys.KEY_PASSWORD).apply();
        saved_username = "";
        saved_password = "";
        if (mConnection != null && mConnection.isConnected())
        {
            mConnection.disconnect();
        }
        Intent intent = new Intent(context, WSService.class);
        intent.setAction(ACTION_DISCONNECT);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_CONNECT.equals(action)) {
                handleActionConnect();
            } else if (ACTION_DISCONNECT.equals(action)) {
                handleActionDisconnect();
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(TAG, "onTaskRemoved");
    }

    private String getFullUri()
    {
        return "ws://mini-mdt.wheely.com/?username=" + saved_username + "&password=" + saved_password;
    }

    private void sendMyCoords()
    {
        Log.d(TAG, "sendMyCoords");
        JSONObject j = new JSONObject();
        try {
            j.put(PreferencesKeys.KEY_LAT, 0);
            j.put(PreferencesKeys.KEY_LON, 0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Intent intent = buildIntent();
            intent.putExtra(INTENT_PARAM_RESULT, SERVICE_RESULT_ERROR);
            intent.putExtra(INTENT_PARAM_ERROR, SERVICE_ERROR_SENDING);
            sendMessageToLoginActivity(intent);
        }
        mConnection.sendTextMessage(j.toString());
    }

    private Intent buildIntent()
    {
        return new Intent(INTENT_FILTER);
    }

    private void handleActionConnect() {
        Log.d(TAG, "handleActionConnect");
        if (mConnection != null && mConnection.isConnected()) {
            while (mConnection.isConnected())
            {
                sendMyCoords();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            this.stopSelf();
            return;
        }

        if (mConnection == null)
            mConnection = new WebSocketConnection();

        try {
            String uri = getFullUri();
            mConnection.connect(uri, new WebSocketHandler() {

                @Override
                public void onOpen() {
                    Log.d(TAG, "Status: Connected to " + getFullUri());

                    sendMyCoords();
                    Intent intent = buildIntent();
                    intent.putExtra(INTENT_PARAM_RESULT, SERVICE_RESULT_CONNECT);
                    sendMessageToLoginActivity(intent);
                }

                @Override
                public void onTextMessage(String payload) {
                    Log.d(TAG, "Got echo: " + payload);
                    Intent intent = buildIntent();
                    intent.putExtra(INTENT_PARAM_RESULT, SERVICE_RESULT_MESSAGE);
                    intent.putExtra(INTENT_PARAM_DATA, payload);
                    sendMessageToLoginActivity(intent);
                }

                @Override
                public void onClose(int code, String reason) {
                    Log.d(TAG, "Connection lost. code = " + code + " reason: " + reason);
//                    Intent intent = buildIntent();
//                    intent.putExtra(INTENT_PARAM_RESULT, SERVICE_RESULT_ERROR);
                    if (code == 3) {
                        Intent intent = buildIntent();
                        intent.putExtra(INTENT_PARAM_RESULT, SERVICE_RESULT_ERROR);
                        intent.putExtra(INTENT_PARAM_ERROR, SERVICE_ERROR_INVALID_LOGIN_PWD);
                        sendMessageToLoginActivity(intent);
                    } else if (code == 2) {
//                        intent.putExtra(INTENT_PARAM_ERROR, SERVICE_ERROR_HOST_UNRESOLVED);
                    }
//                    sendMessageToLoginActivity(intent);
                }
            });
        } catch (WebSocketException e) {

            Log.d(TAG, e.toString());
        }
        this.stopSelf();
    }

    private void sendMessageToLoginActivity(Intent intent)
    {
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void handleActionDisconnect() {
        if (mConnection != null && mConnection.isConnected())
        {
            mConnection.disconnect();
        }

        Intent intent = buildIntent();
        intent.putExtra(INTENT_PARAM_RESULT, SERVICE_RESULT_DISCONNECT);
        sendMessageToLoginActivity(intent);
    }
}
